const express = require('express');
const dayjs = require('dayjs')
const isLeapYear = require('dayjs/plugin/isLeapYear');
dayjs.extend(isLeapYear);
const app = express();
const port = process.env.PORT ?? 56201;
app.use(express.json());
app.use(express.text());
app.post('/square', (req, res) => {

    //console.log(req.body)
    const n = Number(req.body);
    if (isNaN(n)){
        return res.status(400).send('It is not a number');
    }
    const result = n*n;
    res.status(200).send({
        "number": n,
        "square": result
    });
});

app.post('/reverse', (req, res) => {
    const text = req.body;
    const reversed = text.split('').reverse().join('');
    res.send(reversed);
});

const week = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

app.get('/date/:year/:month/:day', (req, res) => {
    const now = dayjs().startOf('day');
    const year = req.params.year;
    const month = --req.params.month;
    const day = req.params.day;
    const date = dayjs(new Date(year, month, day));
    res.send({
        'weekDay': week[date.day()],
        'isLeapYear': date.isLeapYear(),
        'difference': Math.abs(Number(now.diff(date, 'day')))
    })
});

app.listen(port, () => {
    console.log(`port: ${port}`);
})
